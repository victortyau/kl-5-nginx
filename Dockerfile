FROM nginx:alpine

COPY web/ /usr/share/nginx/site

COPY nginx.conf /etc/nginx/conf.d/site.conf

EXPOSE 90
